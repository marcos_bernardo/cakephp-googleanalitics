# Project Reference  #
 [https://github.com/Zehir/CakePHP-Plugin-GoogleAPI](https://github.com/Zehir/CakePHP-Plugin-GoogleAPI)


# Google APIs Client Library for CakePHP # - Analytics

## Description ##
The Google API Client Library enables you to work with Google APIs Analytics . And this plugin facilitates its integration into CakePHP.

**TO-DO** : Implement others services api : YouTube, Google+ , Calendar, and others;

The pull-request are accepted.
You can send me a e-mail or private message.

## Requirements ##
* For [Google API Client Library](https://github.com/google/google-api-php-client)
	* [PHP 5.2.1 or higher](http://www.php.net/)
	* [PHP JSON extension](http://php.net/manual/en/book.json.php)
* For my plugin
	 *  Cakephp 2.x
	 *  Nothing more

## Developer Documentation ##
http://developers.google.com/api-client-library/php
See the bottom of the page for the plugin documentation

## Installation ##
The plugin is pretty easy to set up, all you need to do is to copy it to you application plugins folder.
Be careful, it is important that the plugin **is named GoogleAPI (app/Plugin/GoogleAPI)**

** keyfiles.p12** - Generate file in  [Google Console](https://console.developers.google.com/)
*Create the folder  webroot/GoogleAPI/keys in app/Plugin/GoogleAPI/
result:  app/Plugin/GoogleAPI/webroot/GoogleAPI/keys*

## Configuration ##
Create the file Plugin/GoogleAPI/core.php use core.php.default as template.
The minimals options ares:
* client.ApplicationName: Your app name
* client.DeveloperKey: You developer key.
They are defined even if the configuration file is empty

You can define options in core.php and in controller when you add the component.
The priority is:
* Default configuration
* File core.php
* Components definition

## Basic Example ##
A basic use in core.php

```
#!php
<?php
$config = array('GoogleAPI'=>array(
	/**
	* `client` define the options of client
	*/
	'client' => array(
		'ApplicationName' => 'Your-App-Name',
		'DeveloperKey' => 'DEV-KEY',
                'client_id' => 'CLIENT-ID',
                'service_account_name' => 'SERVICE-ACCOUNT-NAME',
                'keyfile' => 'keyfile.p12', //defaultf  app/Plugin/GoogleAPI/webroot/GoogleAPI/keys
                'redirect_url' => '',
                'client_secret' => 'notasecret'
	),
	'Service' =>array('Analytics')
));

```


A basic use in a controller
```PHP
<?php
App::uses('AppController', 'Controller');
class TestsController extends AppController
{
	public $components = array('Paginator',
		'GoogleAPI.GoogleAPI' => array(
                        'client' => array(
                                'ApplicationName' => 'Painel-Analytics-CakePHP',
                                'ClientId' => 'Client-ID',
                                'service_account_name' => 'SERVICE-ACCOUNT-NAME',
                                'keyfile' => 'keyfile.p12',
                                'RedirectUri' => '',
                                'ClientSecret' => 'notasecret',
                                'AccessType' => 'offline_access'
                        ),
			'Service' => array(
				'Analytics'
			)
		)
	);
	
	public function index() {
		$this->autoRender = false;
        	$this->layout = false;
         	$client = $this->GoogleAPI->Service['Analytics'];
            
            ////  Important!
            // Your analytics profile id. (Admin -> Profile Settings -> 3rd Column Views -> Views ID)
            ////
            	$analytics_id   = 'ga:XXXXXXXX';
            	$lastWeek       = date('Y-m-d', strtotime('-1 month'));
            	$today             = date('Y-m-d');

            	try {
                	$results = $client->data_ga->get($analytics_id, $lastWeek,$today,'ga:newUsers,ga:sessions');
                  //  echo var_dump($results);
                echo '<b>Number of visits this month:</b> ';
                echo $results['totalsForAllResults']['ga:newUsers'].'<br>';
                echo '<b>Number of sessions this month:</b> ';
                echo $results['totalsForAllResults']['ga:sessions'].'<br>';
                echo '<b>% of newvisits per sessions this month:</b> ';
                    echo (($results['totalsForAllResults']['ga:newUsers'] / $results['totalsForAllResults']['ga:sessions'])*100).'% ';

            } catch(Exception $e) {
                echo 'There was an error : - ' . $e->getMessage();
            }
	}
}
```

## Documentation ##

### Basic uses ###
To load a service put this in your $components value.
```PHP
public $components = array(
	'GoogleAPI.GoogleAPI' => array(
		'Service' => array(
			'Analytics'
		)
	)
);
```
You can load multiple services at the same time, in this case the same client will be used.
Then use the array Service to use the API.
Use the google api documentation for list of available functions.
```PHP
$this->GoogleAPI->Service['Analytics'];
```
[See all in Google Developers Guide here](https://code.google.com/p/google-api-php-client/source/browse/#svn%2Ftrunk%2Fexamples%2Fanalytics%253Fstate%253Dclosed)

This is the basic exemple from google api github for Youtube
```PHP
<?php
  require_once 'Google/Client.php';
  require_once 'Google/Service/Books.php';
  $client = new Google_Client();
  $client->setApplicationName("Client_Library_Examples");
  $client->setDeveloperKey("YOUR_APP_KEY");
  $service = new Google_Service_Books($client);
  $optParams = array('filter' => 'free-ebooks');
  $results = $service->volumes->listVolumes('Henry David Thoreau', $optParams);

  foreach ($results as $item) {
    echo $item['volumeInfo']['title'], "<br /> \n";
  }
```
To get the same with my plugin (define Books service in components)
```PHP
$optParams = array('filter' => 'free-ebooks');
$results = $this->GoogleAPI->Service['Books']->volumes->listVolumes('Henry David Thoreau', $optParams);
foreach ($results as $item) {
	echo $item['volumeInfo']['title'], "<br /> \n";
}
```


### Advanced uses ###
If you want to manually load classes, use this function:
```PHP
GoogleAPI::import('Google/Client');
```